<!DOCTYPE html>
<html ng-app="notesApp">
<head>
    <title>Notes App Form</title>
</head>
<body ng-controller="MainCtrl as ctrl">
<form ng-submit="ctrl.submit()" name="myForm">
    <div>
        <h2>Your name and surname</h2>
        <div>
            <label>Name: </label>
            <input name="name"
                   type="text"
                   ng-model="ctrl.user.name"
                   required>
            <label>Surname: </label>
            <input name="surname"
                   type="text"
                   ng-model="ctrl.user.surname"
                   required>
        </div>
        <div>
            <select ng-model="ctrl.user.country" ng-options="c.id as c.label for c in ctrl.countries"></select>
        </div>
        <div>
            <h2>Gender</h2>
            <div ng-repeat="gender in ctrl.genders">
                <label ng-bind="gender.label"></label>
                <input type="radio" name="gender" ng-model="ctrl.user.gender" ng-value="gender.id">
            </div>
        </div>
        <h2>What are your favourite sports?</h2>
        <div ng-repeat="sport in ctrl.user.sports">
            <label ng-bind="sport.label"></label>
            <input type="checkbox" name="sports[]" ng-model="sport.selected" ng-checked="sport.selected">
        </div>
        <div>
            <textarea name="description"
                      ng-model="ctrl.user.description">
        </textarea>
        </div>
        <input type="submit" value="Save">
    </div>
</form>
<script src="../node_modules/angular/angular.min.js"></script>
<script type="text/javascript">
    angular.module('notesApp', [])
        .controller('MainCtrl', [function () {
            var self = this;
            self.genders = [
                {id: 1, label: 'Female'},
                {id: 2, label: 'Male'},
                {id: 3, label: 'Other'}
            ];
            self.countries = [
                {id: 1, label: 'Turkey'},
                {id: 2, label: 'Greece'}
            ];
            self.user = [];
            self.user.name = 'Gokhan';
            self.user.surname = 'Ustuner';
            self.user.gender = 3;
            self.user.country = 2;
            self.user.description = 'Sample Text';
            self.user.sports = [
                {id: 1, label: 'Basketball', selected: false},
                {id: 2, label: 'Football', selected: true},
                {id: 3, label: 'Swimming', selected: true}
            ];
            self.submit = function () {
                console.log(self.user);
            }
        }]);
</script>
</body>
</html>