<!DOCTYPE html>
<html ng-app="notesApp">
<body ng-controller="MainCtrl as ctrl">
<h1>{{ ctrl.helloMessage }}</h1>
<h2>{{ ctrl.goodbyeMessage }}</h2>
<script src="../node_modules/angular/angular.min.js"></script>
<script type="text/javascript">
    angular.module('notesApp', [])
        .controller('MainCtrl', [function () {
            this.helloMessage = 'Hello';
            this.goodbyeMessage = 'Goodbye';
            var goodbyeMessage = 'Goodbye';
        }]);
</script>
</body>
</html>