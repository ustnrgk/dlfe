angular.module('notesApp', [])
    .controller('MainCtrl', [function() {
        var self = this;
        self.tab = 'first'; self.open = function(tab) {
            self.tab = tab;
        };
    }])
    .controller('SubCtrl', ['$log', '$location', '$http', function($log, $location, $http) {
        var self = this; self.list = [
            {id: 1, label: 'Item 0'},
            {id: 2, label: 'Item 1'}
        ];
        self.add = function() {
            $log.log('Button was pressed.');
            // $log.log($location.url());
            // $log.log($location.absUrl());
            // $log.log($location.path());
            // $log.log($http);
            $http.get('http://127.0.0.1:8000/country/217').then(function (response) {
                $log.log(response.data.data);
            });
            self.list.push({
                id: self.list.length + 1,
                label: 'Item ' + self.list.length
            });
        };
    }]);