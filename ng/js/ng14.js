angular.module('notesApp', [])
    .controller('MainCtrl', [function () {
        var self = this;
        self.tab = 'first';
        self.open = function (tab) {
            self.tab = tab;
        };
    }])
    .controller('SubCtrl', ['ItemService', function (ItemService) {
        var self = this;
        self.list = function () {
            return ItemService.list();
        };
        self.add = function () {
            return ItemService.add({
                id: self.list().length + 1,
                label: 'Item ' + (self.list().length + 1)
            });
        };
    }]).factory('ItemService', [function () {
        var items = [
            {id: 1, label: 'Item 1'},
            {id: 2, label: 'Item 2'}
        ];

        return {
            list: function () {
                return items;
            },
            add: function (item) {
                return items.push(item);
            }
        }
}]);