angular.module('countriesApp', [])
    .controller('ListCountries', ['$http', function ($http) {
        var self = this;
        self.errorMessage = '';
        self.countries = [];
        $http.get('/dream_league/ng/sample-response.php').then(function (response) {
            if (response.data.success) {
                self.countries = response.data.countries;
            } else {
                self.errorMessage = response.data.error;
            }
        }, function (errResponse) {
            self.errorMessage = errResponse;
        });
    }]);