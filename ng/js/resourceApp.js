angular.module('resourceApp', ['ngResource'])
    .factory('CountryService', ['$resource', function ($resource) {
        return $resource('/ng/sample-response.php');
    }]);