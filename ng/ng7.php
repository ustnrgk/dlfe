<!DOCTYPE html>
<html ng-app="notesApp">
<head>
    <title>Notes App</title>
</head>
<body ng-controller="MainCtrl as ctrl">
<div ng-repeat="(author, note) in ctrl.notes">
    <div>First Element: {{$first}}</div>
    <div>Middle Element: {{$middle}}</div>
    <div>Last Element: {{$last}}</div>
    <div>Index of Element: {{$index}}</div>
    <div>At Even Position: {{$even}}</div>
    <div>At Odd Position: {{$odd}}</div>
    <span class="author" ng-bind="author"></span>
    <span class="label" ng-bind="note.label">
    </span>
</div>
<script src="../node_modules/angular/angular.min.js"></script>
<script type="text/javascript">
    angular.module('notesApp', [])
        .controller('MainCtrl', [function () {
            var self = this;
            self.notes = {
                Syham: {
                    id: 1,
                    label: 'First Note',
                    done: false
                },
                Misko: {
                    id: 3,
                    label: 'Last Note',
                    done: true
                },
                Brad: {
                    id: 2,
                    label: 'Second Note',
                    done: false
                }
            }
        }]);
</script>
</body>
</html>