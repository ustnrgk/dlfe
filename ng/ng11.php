<!DOCTYPE html>
<html ng-app="notesApp">
<head>
    <title>Notes App Form</title>
    <style>
        .username.ng-valid {
            background-color: green;
        }

        .username.ng-dirty.ng-invalid-required {
            background-color: red;
        }

        .username.ng-dirty.ng-invalid-minlength,
        .username.ng-dirty.ng-invalid-maxlength {
            background-color: lightpink;
        }

        .password.ng-valid {
            background-color: green;
        }

        .password.ng-dirty.ng-invalid-required {
            background-color: red;
        }

        .password.ng-dirty.ng-invalid-minlength {
            background-color: lightpink;
        }

        .date.ng-valid {
            background-color: green;
        }

        .date.ng-dirty.ng-invalid,
        .date.ng-dirty.ng-invalid-required
        {
            background-color: red;
        }
    </style>
</head>
<body ng-controller="MainCtrl as ctrl">
    <form ng-submit="ctrl.submit()" name="myForm">
        <input type="text"
               ng-model="ctrl.user.username"
               required
               ng-minlength="4"
               ng-maxlength="8"
               name="username"
               class="username">
        <span ng-show="myForm.username.$error.required">
            <br>
            Username field is required.
        </span>
        <span ng-show="myForm.username.$error.minlength">
            <br>
            Minimum length required is 4.
        </span>
        <span ng-show="myForm.username.$error.maxlength">
            <br>
            Maximum length is 8.
        </span>
        <br>
        <input type="password"
               ng-model="ctrl.user.password"
               required
               ng-minlength="4"
               name="password"
               class="password">
        <span ng-show="myForm.password.$error.required">
            <br>
            Password field is required.
        </span>
        <span ng-show="myForm.password.$error.minlength">
            <br>
            Minimum length required is 4.
        </span>
        <br>
        <input type="date"
               ng-model="ctrl.user.date"
               required
               name="date"
               class="date">
        <span ng-show="myForm.date.$error.required">
            <br>
            Date is required.
        </span>
        <span ng-show="myForm.date.$invalid">
            <br>
            Date is invalid.
        </span>
        <br>
        <input type="submit"
               value="submit"
                ng-disabled="myForm.$invalid">
    </form>
<script src="../node_modules/angular/angular.min.js"></script>
<script type="text/javascript">
    angular.module('notesApp', [])
        .controller('MainCtrl', [function () {
            var self = this;
            self.submit = function () {
                console.log('User clicked to submit with ', self.user);
            }
        }]);
</script>
</body>
</html>