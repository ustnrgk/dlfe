<!DOCTYPE html>
<html ng-app="notesApp">
<body ng-controller="MainCtrl as ctrl">
<h1>{{ ctrl.message }}</h1>
<button ng-click="ctrl.changeMessage()">Change Message</button>
<script src="../node_modules/angular/angular.min.js"></script>
<script type="text/javascript">
    angular.module('notesApp', [])
        .controller('MainCtrl', [function () {
            var self = this;
            self.message = 'Hello';
            self.changeMessage = function () {
                self.message = 'Goodbye';
            }
        }]);
</script>
</body>
</html>