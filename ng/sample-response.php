<?php
header('Content-Type: application/json');
$data = array(
    'success' => true,
    'countries' => array(
        array('id' => 1, 'name' => 'Turkey', 'population' => 75000000),
        array('id' => 2, 'name' => 'U.S.A', 'population' => 250000000)
    )
);
echo json_encode($data);