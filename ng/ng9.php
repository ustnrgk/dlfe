<!DOCTYPE html>
<html ng-app="notesApp">
<head>
    <title>Notes App</title>
    <style>
        .done {
            background-color: green;
        }

        .pending {
            background-color: red;
        }
    </style>
</head>
<body ng-controller="MainCtrl as ctrl">
    <table>
        <tr ng-repeat-start="note in ctrl.notes">
            <td ng-bind="note.label"></td>
        </tr>
        <tr ng-bind="note.id"></tr>
        <tr ng-class="ctrl.getDoneClass(note.done)" ng-repeat-end>
            <td>Done: {{ note.done }}</td>
        </tr>
    </table>
    <script src="../node_modules/angular/angular.min.js"></script>
    <script type="text/javascript">
        angular.module('notesApp', [])
            .controller('MainCtrl', [function () {
                var self = this;
                self.notes = [
                    {id: 1, label: 'First Note', done: false},
                    {id: 2, label: 'Second Note', done: false},
                    {id: 3, label: 'Third Note', done: true}
                ];
                self.getDoneClass = function (done) {
                    return {
                        done: done,
                        pending: !done
                    }
                };
            }]);
    </script>
</body>
</html>