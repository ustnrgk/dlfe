<!DOCTYPE html>
<html ng-app="notesApp">
<head>
    <title>Notes App</title>
    <style>
        .done {
            background-color: green;
        }

        .pending {
            background-color: red;
        }
    </style>
</head>
<body ng-controller="MainCtrl as ctrl">
<table>
    <thead>
        <tr>
            <th>Id</th>
            <th>Label</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="note in ctrl.notes">
            <td ng-bind="note.id"></td>
            <td ng-bind="note.label"></td>
            <td ng-class="ctrl.getDoneClass(note.done)" ng-bind="note.done"></td>
        </tr>
    </tbody>
</table>
<script src="../node_modules/angular/angular.min.js"></script>
<script type="text/javascript" src="js/notesApp.js"></script>
</body>
</html>