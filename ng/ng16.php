<html ng-app="countriesApp">
<head>
    <title>Countries</title>
    <script type="text/javascript" src="../node_modules/angular/angular.min.js"></script>
    <script type="text/javascript" src="js/ng16.js"></script>
</head>
<body ng-controller="ListCountries as listCountries">
    <h1>Countries List</h1>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>French Name</th>
                <th>Locale Name</th>
                <th>Population</th>
            </tr>
        </thead>
        <tbody>
        <tr ng-repeat="country in listCountries.countries">
            <td ng-bind="country.id"></td>
            <td ng-bind="country.name"></td>
            <td ng-bind="country.nameFr"></td>
            <td ng-bind="country.nameLocale"></td>
            <td ng-bind="country.population"></td>
        </tr>
        </tbody>
    </table>
</body>
</html>